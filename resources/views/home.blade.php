@extends('spark::layouts.app')

@section('content')
<home :user="user" inline-template>
    <div class="container">
        <!-- Application Dashboard -->
        <!-- Row -->
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <div class="card">
                    <div class="card-body">
                        <h4 class="panel-heading">Repos Home</h4>
                        <div class="table-responsive m-t-20">
                            <repo-list></repo-list>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Row -->
    </div>
</home>
@endsection
