Vue.component('home', {
    props: ['user'],

    mounted() {
    }
});

Vue.component('repo-list', {
    template:
    `<table class="table stylish-table">
        <thead>
            <tr>
                <th>Name</th>
                <th colspan="2">Owner</th>
            </tr>
        </thead>
        <tbody>
            <tr v-for="repo in repos" @click="goTo(repo.id)">
                <td><a :href="'/dashboard/' + repo.id">{{ repo.name }}</a></td>
                <td style="width:50px;">
                    <span class="round"><img :src="repo.owner_photo_url" alt="user" width="50"></span>
                </td>
                <td>
                    <h6>{{ repo.owner_name }}</h6>
                </td>
                </a>
            </tr>
        </tbody>
    </table>`,

    data() {
        return {
            repos: []
        };
    },

    filters: {
        truncate: function(string) {
            return string.substring(0, 1);
        }
    },

    methods: {
        goTo(page) {
            window.location.href = '/dashboard/' + page;
        }
    },

    created() {
        axios.get('/repos').then(response => (this.repos = response.data));
    }

});

Vue.component('repo-name', {
    template: '<li><slot></slot></li>'
});


Vue.component('people-presence', {
    template:
            `<div v-if="people.length > 1" class="p-2">
                <h6>Viewing now:</h6>
                <span v-for="person in viewersExceptMe" class="round">
                    <img :src="person.photo_url" alt="user" width="50" :title="person.name">
                    <h6>{{ person.name }}</h6>
                </span>
            </div>`,

    data() {
        return {
            people: []
        };
    },

    computed: {

        viewersExceptMe: function () {
            return this.people.filter(person => person.id !== this.spark.userId);
        }

    },

    methods: {

        // Listen to Echo channels
        listen() {
            //  Presence is split to a separate channel, but this can be done on the same channel
            //  Broadcast to a Presence channel and the other events will also work
            Echo.join('Repo.Presence.' + Spark.route.repo.id)
                .here(viewers => {
                    // Display the people that are viewing the repo
                    this.people = viewers;
                })
                .joining(viewers => {
                    // Update the people that are viewing the repo by adding people that join
                    this.people.push(viewers);
                })
                .leaving(viewers => {
                    // Update the people that are viewing by removing people that leave
                    // this.people.splice(this.people.indexOf(viewers.id), 1); --> might also work; not tested yet
                    this.people = this.people.filter(person => person.id !== viewers.id);
                });
        },

    },

    mounted() {
        this.listen();
    }

});

