<?php

namespace App\Providers;

use Laravel\Spark\Spark;
use Laravel\Spark\Providers\AppServiceProvider as ServiceProvider;

class SparkServiceProvider extends ServiceProvider
{
    /**
     * Your application and company details.
     *
     * @var array
     */
    protected $details = [
        'vendor' => 'Voicebits Apps',
        'product' => 'LEARN X',
        'street' => 'PO Box 111',
        'location' => '2200 Copenhagen N Denmark',
        'phone' => '+45 55 55 55 55',
    ];

    /**
     * The address where customer support e-mails should be sent.
     *
     * @var string
     */
    protected $sendSupportEmailsTo = null;

    /**
     * All of the application developer e-mail addresses.
     *
     * @var array
     */
    protected $developers = [
        'kevinlhall72@gmail.com',
    ];

    /**
     * Indicates if the application will expose an API.
     *
     * @var bool
     */
    protected $usesApi = true;

    /**
     * Register any Spark application services.
     */
    public function register()
    {
        Spark::referToTeamAs('repo');
    }

    /**
     * Finish configuring Spark for the application.
     */
    public function booted()
    {
        \Laravel\Cashier\Cashier::useCurrency('dkk', 'DKK');

        //Spark::identifyTeamsByPath();

        Spark::useStripe()->noCardUpFront()->trialDays(10);

        Spark::freePlan()
            ->features([
                'First', 'Second', 'Third',
            ]);

        Spark::plan('Basic', 'provider-id-1')
            ->price(10)
            ->features([
                'First', 'Second', 'Third',
            ]);
    }
}
