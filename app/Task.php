<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'body',
        'team_id',
        'completed',
    ];

    /**
     * Get the project team repository that owns the task.
     */
    public function repo()
    {
        return $this->belongsTo('App\Team');
    }
}
