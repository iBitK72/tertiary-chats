<?php

namespace App;

use Laravel\Spark\Team as SparkTeam;

class Team extends SparkTeam
{
    /**
     * Get the tasks for the project team repository.
     */
    public function tasks()
    {
        return $this->hasMany('App\Task');
    }
}
