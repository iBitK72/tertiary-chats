<?php

namespace App\Http\Controllers;

use App\Team;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('auth');

        // $this->middleware('subscribed');
    }

    /**
     * Show the application navigation home.
     *
     * @return Response
     */
    public function show()
    {
        return view('home');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Added request to params limit tasks to user's current teams - ie. "repos"
        // Retrieve all Repos and the Owner Name of each Repo the user has access to
        foreach ($request->user()->teams as $team) {
            $repos[] = [
                'id' => $team->id,
                'name' => $team->name,
                'owner_name' => $team->owner->name,
                'owner_photo_url' => $team->owner->photo_url,
            ];
        }

        return $repos;
    }

    /**
     * Show the repo (ie. Team) dashboard.
     *
     * @return Response
     */
    public function dashboard(Team $repo, Request $request)
    {
        abort_unless($request->user()->onTeam($repo), 404);

        $request->user()->switchToTeam($repo);

        return view('dashboard', ['repoName' => $repo->name]);
    }
}
