<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@show');

Route::get('/home', 'HomeController@show');
Route::get('/repos', 'HomeController@index');
Route::get('/dashboard/{repo}', 'HomeController@dashboard');

Route::get('login/{provider}', 'Auth\RegisterController@redirectToProvider');
Route::get('auth/{provider}/callback', 'Auth\RegisterController@handleProviderCallback');

Route::get('/tasks', 'TasksController@index');
Route::post('/tasks', 'TasksController@store');
Route::post('/task/completed/{task}', 'TasksController@completed');

class Order
{
    public $id;

    public function __construct($id)
    {
        $this->id = $id;
    }
}

Route::get('/test', function () {
    $task = \App\Task::create([
            'team_id' => request()->user()->currentTeam->id,
            'body' => 'request->body',
            'title' => 'test delete',
            'completed' => 0,
        ]);

    dd($task);

    $users = \App\User::all();
    foreach ($users as $user) {
        echo $user->onTeam(5).' on team 5 and '.$user->id.' plus '.$user->photo_url.'</br>';
    }

    $allCurrUserTeams = request()->user()->teams;
    dd(request()->user()->id, request()->user()->onTeam(5));
    foreach ($allCurrUserTeams as $team) {
        dd($team, $team->owner);
        $repos[] = [
                'id' => $team->id,
                'name' => $team->name,
                'owner_name' => $team->owner->name,
                'owner_photo_url' => $team->owner->photo_url,
            ];
    }

    $users = \App\User::all();
    foreach ($users as $user) {
        echo $user->id.' plus '.$user->photo_url.'</br>';
    }
    dd(request()->user()->photo_url, $users);
    $result = (request()->user()->onTeam(2)) ? 'True' : 'False';

    return $result;
    $currentTeam = request()->user()->currentTeam;
    $currTeamTasks = $currentTeam->tasks->sortByDesc('updated_at');
    //\App\Team::where('id', '=', $currentTeam->id)->tasks->lastest();
    dd($currTeamTasks);
    dd(\App\Team::find(request()->user()->currentTeam->id)->tasks->latest()->first());
    $allCurrUserTeams = request()->user()->teams;
    $ownedTeams = \App\Team::with('owner')->where('owner_id', '=', request()->user()->id)->get();

    //dd($allCurrUserTeams, $allCurrUserTeams[0]->owner->name, $ownedTeams, $ownedTeams[0]->owner->name);

    $repo = \App\Team::find(request()->user()->currentTeam->id);
    $teams = \App\Team::all();
    //dd($teams);
    foreach ($teams as $team) {
        echo $team->owner_id.' owns '.$team->name.'</br>';
        echo '</br>';
    }

    //dd($repo, $repo->tasks->pluck('title'));

    $users = \App\User::all();
    foreach ($users as $user) {
        foreach ($user->teams as $team) {
            echo $user->name.' is on '.$team->name.' owned by '.$team->owner->name;
            echo '</br>';
            $repos[] = ['name' => $team->name, 'owner_name' => $team->owner->name];
        }
    }
    dd($repos);
    //OrderStatusUpdated::dispatch(new Order(7));
    //
    // get tasks
    $taskInfo['repoId'] = request()->user()->currentTeam->id;
    $taskInfo['tasks'] = \App\Task::where('team_id', '=', $taskInfo['repoId'])->latest()->get();

    return $taskInfo;
    // dd(\App\Team::find(request()->user()->currentTeam->id)->tasks);
    $currentTeam = request()->user()->currentTeam;
    $currTeamTasks = $currentTeam->tasks->sortByDesc('updated_at');
    $currTeamTasks = \App\Task::where('team_id', '=', $currentTeam->id)->latest()->get();
    $currTeamTasks = \App\Task::where('team_id', '=', request()->user()->currentTeam->id)->latest()->get();
    dd($currTeamTasks);

    return $currTeamTasks;

    return \App\Team::find(request()->user()->currentTeam->id)->tasks->sortByDesc('updated_at');
    //return \App\Team::find(request()->user()->currentTeam->id)->tasks->pluck('title');
//    return Task::latest()->pluck('title');
//
//    post tasks
    $task = \App\Task::forceCreate([
        'team_id' => request()->user()->currentTeam->id,
        'body' => request('body'),
        'title' => request('title'),
    ]);

    \App\Events\TaskCreated::dispatch($task);
});
