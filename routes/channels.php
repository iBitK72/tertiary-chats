<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel('Chatty.Tasks.{id}', function ($user, $id) {
    return $user->onTeam($id);
});

Broadcast::channel('Repo.Presence.{id}', function ($user, $id) {
    if ($user->onTeam($id)) {
        return ['id' => $user->id, 'name' => $user->name, 'photo_url' => $user->photo_url, 'team_id' => $id];
    }

    return false;
});

Broadcast::channel('App.User.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
});
